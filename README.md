# Hahow project #

## 簡介 ##

這份 README 會詳細說明我對 Hahow 專案的實作方式與架構，由於內容有點多，下方有目錄可以快速到想要了解的部分。感謝面試邀約，那就開始吧！

## 目錄 ##

- [該如何跑起這個 server](README.md#該如何跑起這個-server)
- [專案跟API server 的架構邏輯](README.md#專案跟api-server-的架構邏輯)
- [專案內所用到的第三方 Library](README.md#專案內所用到的第三方-library)
- [在程式碼中寫註解的原則](README.md#在程式碼中寫註解的原則)
- [在這份專案中遇到的困難](README.md#在這份專案中遇到的困難)
- [各個 route 對於接收資料的各種邊際情況的處理](README.md#各個-route-對於接收資料的各種邊際情況的處理)
- [程式的可讀性與可維護性](README.md#程式的可讀性與可維護性)
- [如果這不只是一份作業專案而是一份正式開發的需求呢](README.md#如果這不只是一份作業專案而是一份正式開發的需求呢)
- [哪些地方可以改進](README.md#哪些地方可以改進)
- [結語](README.md#結語)

## 該如何跑起這個 server ##

1. ### 前置需求 ###

    在運行此專案的後端之前，請確保擁有以下運行環境：

    - **Node.js 20.10.0 或更高版本：** 您可以從 [Node.js 官方網站](https://nodejs.org/) 下載並安裝適用的版本。

2. ### clone 專案 ###

    使用以下指令將專案 clone 到你的電腦：

    ```
    git clone https://gitlab.com/liao00428/hahow-project.git

    cd hahow-project
    ```

3. ### 設定環境變數 ###

    為了讓後端在不同環境中順利運行，需要設置以下環境變數。這些變數在應用程式啟動時會被載入。

    請先建立一個 `.env` 檔

    ```
    touch .env
    ```

    請確保在 `.env` 檔案中正確設置這些變數，根據實際情況調整這些變數的值。

    - **ENVIRONMENT**：應用程式運行的環境，本地開發環境請使用 `localhost`。

    - **PORT**：應用程式運行的 port 號。

    - **CLIENT_HOST**：前端的主機域名。

    - **HAHOW_API_BASE_URL**：Hahow API 服務的 base URL。

    以下是 `.env` 文件的範例格式，按照這份範例設置就可以啟動 Server：

    ```
    ENVIRONMENT=localhost
    PORT=3000
    CLIENT_HOST=http://localhost:3000
    HAHOW_API_BASE_URL=https://hahow-recruit.herokuapp.com
    ```

4. ### 安裝套件 ###

    運行以下指令，這個指令會根據 `package.json` 安裝所有必要的 npm 套件：

    ```
    npm install
    ```

5. ### 運行開發伺服器 ###

    運行以下指令以啟動開發伺服器。

    ```
    npm run dev
    ```

6. ### 確認 server 運行正常 ###

    發送請求到 `/health` 確認 server 是否運行正常

    ```
    curl -X GET http://localhost:3000/health
    ```

    看是否有顯示當前服務啟動狀態，有的話代表服務正常啟動。

    ```
    Health: Server instance is healthy with process id 86702 on Wed Jul 03 2024 20:41:52 GMT+0800 (Taipei Standard Time)%
    ```

## 專案跟API server 的架構邏輯 ##

- ### 專案資料夾結構規劃 ###

    在資料夾結構規劃上將功能依照不同種類去做模組化。

    ![project-tree.png](./public/project-tree.png)

    - **apidocs**：包含 OpenAPI (Swagger) 文件，用於 API 規格說明。
    - **deploy**：包含 Terraform 檔案，用於雲服務的部署。
    - **public**：包含應用程式的靜態資源。
    - **src**：原始碼目錄。
      - **features/heroes**：英雄功能模組。
        - **controller**：控制器，用於處理 HTTP 請求。
        - **route**：路由定義。
        - **schema**：數據驗證模式。
        - **services**：業務邏輯服務。
      - **global**：全域服務、中介軟體和工具。
        - **HeroesResourcesService**：英雄資源的接口、模式和服務。
        - **middleware**：驗證和模式驗證的中介軟體。
        - **utils**：工具函式，包括自訂錯誤處理。

- ### Backend server 架構 ###

    本專案的後端伺服器使用 Node.js 和 Express.js 進行開發，並遵循 SOLID 原則和策略模式。以下是伺服器的主要架構：

    - **Application Class ( app.js )**

        Application class 負責初始化和配置伺服器。其主要功能包括：

        - **initialize()**：初始化伺服器，包括載入配置、啟動伺服器以及處理退出訊號。
        - **loadConfig()**：驗證和載入環境變數。
        - **startServer()**：創建並啟動伺服器實例。
        - **handleExit()**：處理伺服器退出事件（如 SIGINT 和 SIGTERM 信號）。

    - **HahowProjectServer Class( setupServer.ts )**

        HahowProjectServer class 負責配置 Express 應用程式並啟動 HTTP 伺服器。其主要功能包括：

        - **start()**：啟動伺服器，並依次調用各種中介軟體和路由設定函數。
        - **securityMiddleware()**：配置 CORS 以允許來自特定來源的請求。
        - **standardMiddleware()**：設定 JSON 和 URL-encoded 請求解析器。
        - **performanceMiddleware()**：計算每個 request 的執行時間。
        - **routeMiddleware()**：設定應用程式路由。
        - **apiDocuments()**：在開發環境中載入和顯示 Swagger API 文件。
        - **errorHandler()**：全局錯誤處理，處理未捕獲的錯誤和未找到的路由。
        - **startServer()**：創建 HTTP 伺服器並監聽特定端口。

    - **伺服器啟動**

        在 Application class 中，通過 initialize() 方法載入配置並啟動伺服器。HahowProjectServer 則負責設定各種中介軟體、路由和錯誤處理，最後啟動 HTTP 伺服器。

- ### API server 的架構 ###
  - **API server**

    在原有的 path 的基礎上加上 /api/v1 的前綴，/api 表示這是屬於 API 服務，/v1 則表示這個 API 的版本，未來 API 版本需要更新時就可以將 v1 改為 v2 來表示當前 API 服務的版本。

    因此在 call 這個服務的 API 時需加上 /api/v1 的前綴
    ```
    http://localhost:3000/api/v1/heroes
    http://localhost:3000/api/v1/heroes/:heroId
    ```

    詳細的 API 文件可以再啟動本地 Server 後，透過以下網址進入

    http://localhost:3000/api-docs/

  - **reponse**

    針對 response 回傳的內容我有對 success 跟 error 的狀況制定標準格式

    ```
    // success
    {
        "status": "success",
        "message": <success message>,
        "result": <result>
    }

    // error
    {
        "status": "error"
        "message": <error message>,
        "statusCode": <status code>,
    }

    ```

    使用這種格式的好處是使用這個 API 的開發者只要先判斷 status 是否為 ‘succes’ 或是 ‘error’，就能確定這次的 API request 是否成功。如果 status 為 error，則可以進一步使用 statusCode 來判斷錯誤類型。

## 專案內所用到的第三方 Library ##

以下是我在這個專案所用到的套件

- ### TypeScript ###

    - **typescript**：一個 JavaScript 的超集，增加了靜態類型檢查，使開發更具結構和安全性。
    - **@types/node**：提供 Node.js 的 TypeScript 類型定義。
    - **ts-node-dev**：一個用於開發的工具，可以在 TypeScript 文件改變時自動重新啟動 Node.js 應用。

- ### Express ###

    - **express**：一個輕量級的 Node.js Web 應用框架，用於構建 API 和 Web 應用。
    - **@types/express**：提供 Express 的 TypeScript 類型定義。

- ### Env ###

    - **dotenv**：用於從 .env 文件中加載環境變量，方便配置管理。

- ### HTTP status code ###

    - **http-status-codes**：一個包含所有 HTTP 狀態碼的套件，可以更方便的使用這些 status code。

- ### CORS ###

    - **cors**：用於在 Express 中啟用跨源資源共享 (CORS)。可以通過配置允許的來源、HTTP 方法和標頭，處理預檢請求，實現跨源資源共享。
    - **@types/cors**：提供 CORS 的 TypeScript 類型定義。

- ### Unit test ###

    - **jest**：JavaScript 測試框架，用於 unit test。允許創建模擬函數，隔離並測試程式的特定部分，也有提供詳細的測試報告。
    - **ts-jest**：一個 TypeScript 的 Jest 預設配置，使得在 Jest 中運行 TypeScript 測試更方便。
    - **@types/jest**：提供 Jest 的 TypeScript 類型定義。
    - **nock**：提供在 Node.js 中攔截和模擬 HTTP 請求的功能。捕捉傳出的 HTTP 請求並允許你指定模擬響應，支持對request body、header 的靈活匹配。

- ### Swagger ###

    - **swagger-ui-express**：用於在 Express 中集成 Swagger UI，提供 API 文件。
    - **yaml**：用於解析 YAML 文件，用來讀取 Swagger yaml 文件。
    - **@types/swagger-ui-express**：提供 Swagger UI Express 的 TypeScript 類型定義。

- ### Joi ###

    - **joi**：用於數據驗證和模式定義，確保格式符合預期。它可以使用很簡易的方式定義數據結構和驗證規則，支持內建和自定義驗證方法，提供錯誤信息。

- ### eslint ###

    - **eslint**：一個 JavaScript 和 TypeScript 的靜態程式分析工具，用於確保程式質量和一致性。
    - **eslint-config-standard-with-typescript**：ESLint 的配置，包含標準的 JavaScript 規範和 TypeScript 支持。
    - **eslint-plugin-import**：提供有關 ES6 module 導入的規則。
    - **eslint-plugin-n**：提供有關 Node.js 規則的。
    - **eslint-plugin-promise**：提供有關 Promises 的規則。

## 在程式碼中寫註解的原則 ##

對於註解的看法是應該讓程式碼本身具有最大的可讀性( 前提是確保團隊中的 coding style 一致性 )，減少不必要的註解。

那我會寫註解的情況主要有以下幾個

- ### API 說明 ###

  在 API 的入口為每個 API 提供的說明，包含其用途、參數、回傳值等。

  ![api-comment.png](./public/api-comment.png)

- ### 對意圖的解釋 ###

  解釋某段程式碼的設計意圖，特別是當這段程式碼的邏輯或設計不明顯時，讓其他工程師更容易理解。

  ![explain-comment-1.png](./public/explain-comment-1.png)
  ![explain-comment-2.png](./public/explain-comment-2.png)

- ### 待辦事項 ###

  註明需要進一步完善或改進的地方，方便後續開發和維護。

## 在這份專案中遇到的困難 ##

這份專案中我主要遇到兩個比較重要的問題

- ### Route 同 path 不同權限 ###

  在開發過程中，有注意到需求是使用同個 path，但根據不同權限來回傳不同資料，因此就在思考要如何完成這個需求並確保未來的擴展性，決定採取以下設計方案：

  - **AuthenticateMiddleware**

      在每個 request 進來時，先通過一個 AuthenticateMiddleware 來決定該 request 是否具有驗證權限。在取資料前先確認授權狀態。

  - **工廠模式和策略模式**
  
      資料取得部分使用工廠模式和策略模式來實現。建立兩個資料取得策略：有權限策略和無權限策略。當 request 進來後，使用工廠模式來判斷當前 request 的權限狀態，並回傳對應的資料取得策略。

      ![factory-strategy.png](./public/factory-strategy.png)
  
  未來如果需要新增不同的資料取得策略，只需要在策略工廠中加入新的策略判斷邏輯並回傳對應的策略。這樣就可以方便的添加新策略，而且也不會影響現有邏輯，使未來在擴充資料取得策略時只需進行最小的修改。

- ### Server 環境變數問題 ###

  在建立 CI / CD 流程部署的部分時，有遇到一個問題是要如何將環境變數放到 AWS server 讓 docker compose 可以吃到這個專案需要的環境變數，原先的 docker-compose.yml 設定跟流程是這樣

  - **docker-compose.yml 使用 environment 設定引入環境變數**

    ![docker-compose-1.png](./public/docker-compose-1.png)

  - **gitlab-ci 流程**

      1. 將 GitLab CI/CD 中設定的環境變數輸出成 APP_ENV.sh 檔案。
      2. 使用 SCP 將 APP_ENV.sh 檔案傳輸至 AWS 伺服器。
      3. 執行 source APP_ENV.sh 指令，使檔案中的環境變數在 AWS 伺服器上生效。

  - **結果**
      
    環境變數無法成功地在 AWS 伺服器上生效。


  後面決定改用另一種引入環境變數的方式

  - **docker-compose.yml 修改引入環境變數的方法，變成使用 env_file 的方法引入環境變數。**

    ![docker-compose-2.png](./public/docker-compose-2.png)

  - **gitlab-ci 流程**

      1. 將 GitLab CI/CD 中設定的環境變數輸出成 .env 檔案。
      2. 使用 SCP 將 .env 檔案傳輸至 AWS 伺服器。
      3. 使用 docker compose up 指令部署。

  - **結果**
      
      確認環境變數成功在 AWS 伺服器上生效，順利完成 CI/CD 流程。

## 各個 route 對於接收資料的各種邊際情況的處理 ##

邊際情況我分成 request、reponse 兩種狀況來討論

- ### Request header & parameter ###

  專案的的需求中，會用到 header 來做權限驗證，會用到 url 裡面的 parameter 參數來取得對應的資料，因此有對這兩樣用到的東西做格式判斷。

  - **Header**

    在 Header 方面，這邊是用 joi 去做簡單的格式判斷，判斷 name、password 是否存在，這兩個是否為字串，都符合的話才去做下一階段 call Hahow API 驗證權限，格式不符合的話直接判定這次的 request 是屬於未授權的 request，在取得資料時就回傳未授權的資料。

    ![validate-header.png](./public/validate-header.png)

  - **Parameter**

    在參數處理方面，我們會用到 heroId 這個參數來取得對應的英雄資料。同樣使用 joi 套件對 heroId 的格式進行判斷，主要是檢查 heroId 是否包含空字串。如果發現 heroId 包含空字串，就表示這次的請求是一個 bad request，我們會直接拋出 BadRequestError。

    這邊並沒有針對 heroId 是否為英文或數字做進一步的驗證是因為 id 的格式有可能有很多種，可能是純英文，可能是純數字，也可能是英文加數字，因此這邊就假設 heroId 已經被確保為有效的識別碼，只需要確保它不為空即可。如果需要更嚴謹的判斷的話也可以在這邊加上排除特殊字元的判斷。

    ![validate-parameter.png](./public/validate-parameter.png)

- ### Response data ###

  在透過 Hahow API 取得所需資料時，假設 Hahow API 已經是一個穩定的服務，其資料結構不會有大改變。因此可以針對 Hahow API 的 response 做格式驗證，確保其符合接收要求。如果資料格式不正確，則會 throw Error。

  在測試 Hahow API 的過程中，發現有些情況下，雖然回傳的 HTTP 狀態碼是 200，但內容卻顯示 "backend error"。假設這種情況表示 Hahow API 出現了某些未知問題，導致服務無法使用。這時候應該拋出錯誤通知前端，告知現在服務出現問題。

  這邊使用 joi 套件對 response 的 schema 進行判斷，並搭配 try-catch 來拋出 InternalServerError。這個 InternalServerError 會被 global error handler 捕捉，並回傳給前端對應的 error response，前端則可以根據這個情況向使用者顯示相應的提示畫面。

  ![validate-response.png](./public/validate-response.png)

## 程式的可讀性與可維護性 ##

專案的可讀性跟可維護性我會用以下幾點來探討

- ### 專案結構 ###

    - **目錄結構清晰**

        專案結構按功能模組進行組織，便於理解。( 例如 : features 裡面就是核心功能，內部再根據功能種類去區分，global 裡面就是外部服務或是工具類服務，deploy 則放部署相關檔案。)

        ![folder-tree.png](./public/folder-tree.png)

- ### 程式風格 ###

    - **一致性**

        透過套件確保程式風格一致，符合常見的 JavaScript/Node.js 風格規範( eslint )。

    - **Dependency Injection**
      
        在 class 的寫法中使用 Dependency Injection 來降低 class 之間的耦合度。這讓 class 更容易測試和擴展。

    - **命名規範**

        變數、函數和類的命名具有描述性，不會是無意義的命名，透過名稱就能清楚的表達用途。( 例如 : get 開頭的就表示取得資料，is 開頭的則是 boolean，用來當作判斷條件。)

    - **註解**

        關鍵程式段落有寫註解，幫助理解程式邏輯。

    - **設計模式**

        這份專案有用到不同的設計模式如工廠模式以及策略模式，增強程式適應變化的能力

- ### 模組化 ###

    - **模組化**

        每個特定功能都有各自的 class 將不同的功能模組化，這樣設計使得修改某個功能時，只需改動相應的 class，不會影響到其他功能，提高了系統的可維護性和可擴展性。未來如果系統變得複雜，需要將功能拆出去變成微服務的設計時也可以很好的去拆分功能。

    - **SOLID 原則**

        程式設計上有儘量遵循 SOLID 原則，提高程式的靈活性和可擴展性。

- ### 錯誤處理 ###

    - **全域錯誤處理**

        建立 global error handler，統一管理錯誤，提高程式的穩定性和可維護性。

    - **自定義錯誤類別**

        自定義多種錯誤類別，讓錯誤信息更具描述性和可追溯性。

        ![error-handler.png](./public/error-handler.png)
        ![custom-error.png](./public/custom-error.png)
- ### 測試 ###

    - **單元測試**

        對於核心功能進行了單元測試，包含正向測試以及反向測試，提高測試覆蓋率確保程式的可靠性。
    
    - **整合測試**

        對於核心功能以及串接到的其他模組進行整合測試，驗證系統模組之間的協作功能。

        ![unit-test-coverage.png](./public/unit-test-coverage.png)

## 如果這不只是一份作業專案而是一份正式開發的需求呢 ##

假設這是一份正式的開發需求，我負責後端開發，開發出來的 API 需要讓前端來串接。

為了讓專案更完整，我為這個專案加入了下面這些部分：

- ### Cloud infrastructure ###

    建立雲端基礎設施，讓這份服務可以在上面運行，節省自行管理和維護主機的成本。
    這邊我在 AWS 上建立 Cloud 基礎服務，包含 VPC、EC2*2 ( 1台給 dev 環境，1台給 prod 環境 )。
    建立雲端架構的工具是使用 Terraform 來建立，使用 IaC 工具的好處可以確保環境配置一致，方便管理。

    ![aws-infrastructure.png](./public/aws-infrastructure.png)

- ### DNS record ### 
  
    為這個服務設定 DNS record，方便前端進行串接。
    DNS record 的部分我選擇使用 Terraform 在 Cloudflare 建立我的 DNS record，沒有選擇使用 AWS Route53 的原因是我需要使用 Cloudflare 的 TLS 服務。
    這邊我自己的 domain 設定為下

    - **Dev** ：https://hahow-project-dev-api.liaospace.com
    - **Prod**：https://hahow-project-api.liaospace.com

    可以透過這個 domain 來觸發 API

    ```
    curl -H "Accept: application/json" -H "Content-Type: application/json" -X GET https://hahow-project-api.liaospace.com/api/v1/heroes
    curl -H "Accept: application/json" -H "Content-Type: application/json" -X GET https://hahow-project-api.liaospace.com/api/v1/heroes/1
    ```

    ![cloudflare-dns.png](./public/cloudflare-dns.png)
    ![dns-record.png](./public/dns-record.png)

- ### TLS ###

    確保前後端傳輸中的資料是有經過安全加密的，因此需要設定 TLS，確保所有的傳輸都是通過 https 進行的。
    這邊我選擇使用 Cloudflare 的 TLS 服務，快速為這份後端專案建立 https。

    ![https.png](./public/https.png)

- ### CORS ###
    在後端配置 CORS，允許前端從不同的 domain 訪問 API。這對前後端分離的架構非常重要，可以防止跨域請求被阻止。
    這邊假設後端的域名的邏輯是在前端的域名加上 '-api' 後綴來表示這個域名屬於 API 服務。為了使前端能夠順利串接後端資料，需要在後端配置 CORS 設置，允許前端能存取後端 API 的資料。

    ![cors.png](./public/cors.png)

- ### CI / CD ### 

    設定完整 CI/CD 流程，自動化構建、測試和部署過程。
    我使用 gitlab-ci 服務來建立完整的 CI / CD 流程，詳細的 yml 文件內容可以看 .gitlab-ci.yml。
    那 CI / CD 的流程如下：
    1. 先對專案做 lint test、unit test 等測試。
    2. 測試完成後將專案 build 成 Docker Image 並存入 gitlab 的 container registry。
    3. 最後再使用 ssh 的方式連進 aws server，並使用 docker-compose 部署

    ![backend-cicd.png](./public/backend-cicd.png)

- ### API Documents ###

    建立完整的 API 文件，讓其他的工程師都可以快速的了解目前專案 API 功能。
    我使用 Swagger 來建立 API 文件，當在 local 啟動 server 時， 可以透過這個網址開啟 API 文件

    ![api-document.png](./public/api-document.png)

- ### Performance Middleware ###

    設定 middleware 負責計算每個 API 請求的處理時間，可以幫助找出性能瓶頸，確保 API 的高效運行。

    ![performance-middleware.png](./public/performance-middleware.png)

## 哪些地方可以改進 ##
由於是以最小可行性的方案來評估這份專案的進行，因此這份專案還有很多可以改進的地方，這邊是我目前所找到未來可以改善的方向。

- ### Dependency Inversion Principle ###

    heroesResourcesService 可以針對 axios 使用 Dependency Inversion Principle 去重構，讓 call API 的部分不依賴 axios。

- ### Log ###

    目前的方案缺乏完善的 Log 記錄和管理機制。未來可加入 prometheus 等工具建立完整的 Log 系統來記錄各種操作、錯誤和性能數據，當專案出現 Bug 時可以更好地監控和排查問題。

- ### TLS ###

    目前的 TLS 協定是透過 Cloudflare proxy 的方式實現的，因此 Cloudflare server 到我們服務這段其實是沒有 TLS 保護的，可以將這段加上完整的 TLS 確保整段的 request 都是有完整的 https。

    ![cloudflare-flex-tls.png](./public/cloudflare-flex-tls.png)

- ### Rate Limit ###

    目前的系統並沒有實施速率限制，這可能會導致系統過載和 DDOS 攻擊風險。可以加入 Rate limit 限制功能，根據用戶和 IP 地址限制 API 請求的頻率，以保護系統資源和穩定性。

- ### Single Point of Failure ###

    目前的設計中存在單點故障的風險。如果服務出現故障，會導致整個系統無法正常運行。可以引入高可用性架構和故障轉移機制像是使用分散式架構以及 K8s 服務等等，提高系統的可靠性和可用性。

## 結語 ##

以上就是我對這次 Hahow 專案的說明，感謝讀到這邊的你 / 妳，希望有機會能夠加入Hahow 謝謝！
