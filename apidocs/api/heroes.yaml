paths:
  /heroes:
    get:
      tags:
        - Heroes
      summary: API to get heroes information, including basic and detailed profiles based on user authorization
      parameters:
        - name: Name
          in: header
          required: false
          schema:
            type: string
          description: User name for authorization
        - name: Password
          in: header
          required: false
          schema:
            type: string
          description: User password for authorization
      responses:
        '200':
          description: Get hero list success
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                  result:
                    type: object
                    properties:
                      heroes:
                        type: array
                        items:
                          type: object
                          properties:
                            id:
                              type: string
                            name:
                              type: string
                            image:
                              type: string
                              format: uri
                            profile:
                              type: object
                              properties:
                                str:
                                  type: number
                                int:
                                  type: number
                                agi:
                                  type: number
                                luk:
                                  type: number
              examples:
                BasicHeroList:
                  summary: Basic hero list (unauthorized)
                  value:
                    message: 'Get hero list success'
                    status: 'success'
                    result:
                      heroes:
                        - id: 'hero1'
                          name: 'Hero One'
                          image: 'https://example.com/path/to/image1.png'
                        - id: 'hero2'
                          name: 'Hero Two'
                          image: 'https://example.com/path/to/image2.png'
                DetailedHeroList:
                  summary: Detailed hero list (authorized)
                  value:
                    message: 'Get hero list success'
                    status: 'success'
                    result:
                      heroes:
                        - id: 'hero1'
                          name: 'Hero One'
                          image: 'https://example.com/path/to/image1.png'
                          profile:
                            str: 10
                            int: 20
                            agi: 15
                            luk: 5
                        - id: 'hero2'
                          name: 'Hero Two'
                          image: 'https://example.com/path/to/image2.png'
                          profile:
                            str: 12
                            int: 18
                            agi: 14
                            luk: 6
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<InternalServerError message>'
                  status: 'error'
                  statusCode: 500
  /heroes/{heroId}:
    get:
      tags:
        - Heroes
      summary: API to get single hero by heroId, including basic and detailed profiles based on user authorization
      parameters:
        - name: heroId
          in: path
          required: true
          schema:
            type: string
          description: The unique identifier of the hero
        - name: Name
          in: header
          required: false
          schema:
            type: string
          description: User name for authorization
        - name: Password
          in: header
          required: false
          schema:
            type: string
          description: User password for authorization
      responses:
        '200':
          description: Get hero by id success
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                  result:
                    type: object
                    properties:
                      id:
                        type: string
                      name:
                        type: string
                      image:
                        type: string
                        format: uri
                      profile:
                        type: object
                        properties:
                          str:
                            type: number
                          int:
                            type: number
                          agi:
                            type: number
                          luk:
                            type: number
              examples:
                BasicHeroList:
                  summary: Basic hero list (unauthorized)
                  value:
                    message: 'Get hero by id success'
                    status: 'success'
                    result:
                      heroes:
                        id: 'hero1'
                        name: 'Hero One'
                        image: 'https://example.com/path/to/image1.png'
                DetailedHeroList:
                  summary: Detailed hero list (authorized)
                  value:
                    message: 'Get hero by id success'
                    status: 'success'
                    result:
                      heroes:
                        id: 'hero1'
                        name: 'Hero One'
                        image: 'https://example.com/path/to/image1.png'
                        profile:
                          str: 10
                          int: 20
                          agi: 15
                          luk: 5
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestError'
                example:
                  message: 'Bad request'
                  status: 'error'
                  statusCode: 400
        '404':
          description: Hero does not exist
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundError'
                example:
                  message: 'Hero with id <heroId> not found'
                  status: 'error'
                  statusCode: 404
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<InternalServerError message>'
                  status: 'error'
                  statusCode: 500
