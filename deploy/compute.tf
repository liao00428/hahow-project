# --- ec2 instance ---
resource "aws_instance" "Hahow_project_development" {
  ami           = "ami-034bc4e4fcccfe844" # Amazon Linux 2 AMI
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.Hahow_project_public_1.id
  vpc_security_group_ids = [
    aws_security_group.Hahow_project_all_out.id,
    aws_security_group.Hahow_project_ssh.id,
    aws_security_group.Hahow_project_http.id,
    aws_security_group.Hahow_project_https.id,
  ]

  key_name = "Hahow_project"

  user_data = <<-EOF
                #!/bin/bash
                yum update -y
                yum install git -y
                # Install Docker
                amazon-linux-extras install docker -y
                sudo usermod -aG docker ec2-user
                # Install Docker Compose
                mkdir -p /home/ec2-user/.docker/cli-plugins
                curl -SL https://github.com/docker/compose/releases/download/v2.28.1/docker-compose-linux-x86_64 -o /home/ec2-user/.docker/cli-plugins/docker-compose
                chmod +x /home/ec2-user/.docker/cli-plugins/docker-compose
                # Start Docker
                systemctl start docker
                systemctl enable docker
              EOF

  tags = {
    Name = "Hahow_project_development_server"
  }
}

resource "aws_instance" "Hahow_project_production" {
  ami           = "ami-034bc4e4fcccfe844" # Amazon Linux 2 AMI
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.Hahow_project_public_1.id
  vpc_security_group_ids = [
    aws_security_group.Hahow_project_all_out.id,
    aws_security_group.Hahow_project_ssh.id,
    aws_security_group.Hahow_project_http.id,
    aws_security_group.Hahow_project_https.id,
  ]

  key_name = "Hahow_project"

  user_data = <<-EOF
                #!/bin/bash
                yum update -y
                yum install git -y
                # Install Docker
                amazon-linux-extras install docker -y
                sudo usermod -aG docker ec2-user
                # Install Docker Compose
                mkdir -p /home/ec2-user/.docker/cli-plugins
                curl -SL https://github.com/docker/compose/releases/download/v2.28.1/docker-compose-linux-x86_64 -o /home/ec2-user/.docker/cli-plugins/docker-compose
                chmod +x /home/ec2-user/.docker/cli-plugins/docker-compose
                # Start Docker
                systemctl start docker
                systemctl enable docker
              EOF

  tags = {
    Name = "Hahow_project_production_server"
  }
}
