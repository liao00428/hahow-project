# --- Cloudflare ---
provider "cloudflare" {
  api_token = var.CLOUDFLARE_API_TOKEN
}

data "cloudflare_zones" "liaospace_public" {
  filter {
    name = var.DOMAIN_NAME
  }
}

resource "cloudflare_record" "Hahow_project_development" {
  zone_id = data.cloudflare_zones.liaospace_public.zones[0].id
  name    = "Hahow-project-dev-api"
  type    = "A"
  ttl     = 1
  value   = aws_instance.Hahow_project_development.public_ip
  proxied = true
}


resource "cloudflare_record" "Hahow_project_production" {
  zone_id = data.cloudflare_zones.liaospace_public.zones[0].id
  name    = "Hahow-project-api"
  type    = "A"
  ttl     = 1
  value   = aws_instance.Hahow_project_production.public_ip
  proxied = true
}
