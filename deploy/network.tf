# --- vpc ---
resource "aws_vpc" "Hahow_project" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "vpc_Hahow_project"
  }
}

# --- internet gateway ---
resource "aws_internet_gateway" "Hahow_project" {
  vpc_id = aws_vpc.Hahow_project.id

  tags = {
    Name = "igw_Hahow_project"
  }
}

# --- subnet ---
# public * 1 for server 
resource "aws_subnet" "Hahow_project_public_1" {
  vpc_id                                      = aws_vpc.Hahow_project.id
  availability_zone                           = "ap-northeast-1a"
  cidr_block                                  = "10.0.0.0/24"
  map_public_ip_on_launch                     = true
  enable_resource_name_dns_a_record_on_launch = true

  tags = {
    Name = "subnet_Hahow_project_public_1"
  }
}

# --- route table & association ---
# public route to internet gateway
resource "aws_route_table" "Hahow_project_public_1" {
  vpc_id = aws_vpc.Hahow_project.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.Hahow_project.id
  }

  tags = {
    Name = "rt_Hahow_project_public_1"
  }
}

resource "aws_route_table_association" "Hahow_project_public_1" {
  subnet_id      = aws_subnet.Hahow_project_public_1.id
  route_table_id = aws_route_table.Hahow_project_public_1.id
}

# --- security group ---
# out
resource "aws_security_group" "Hahow_project_all_out" {
  name        = "Hahow_project_all_out"
  description = "Allow all traffic out"
  vpc_id      = aws_vpc.Hahow_project.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ssh
resource "aws_security_group" "Hahow_project_ssh" {
  name        = "Hahow_project_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.Hahow_project.id

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# http
resource "aws_security_group" "Hahow_project_http" {
  name        = "Hahow_project_http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_vpc.Hahow_project.id

  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# https
resource "aws_security_group" "Hahow_project_https" {
  name        = "Hahow_project_https"
  description = "Allow HTTPS inbound traffic"
  vpc_id      = aws_vpc.Hahow_project.id

  ingress {
    description = "HTTPS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
