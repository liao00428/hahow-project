variable "CLOUDFLARE_API_TOKEN" {
  description = "value of the Cloudflare API token."
  type        = string
}

variable "DOMAIN_NAME" {
  description = "The domain name of the website."
  type        = string
}
