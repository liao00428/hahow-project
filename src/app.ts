import express, { Express } from 'express';
import { config } from './config';
import { HahowProjectServer } from './setupServer';

class Application {
	public initialize(): void {
		this.loadConfig();
		this.startServer();
		this.handleExit();
	}

	private loadConfig(): void {
		config.validateConfig();
	}

	private startServer(): void {
		const app: Express = express();
		const server: HahowProjectServer = new HahowProjectServer(app);
		server.start();
	}

	private handleExit(): void {
		process.on('SIGINT', () => {
			process.exit(0);
		});

		process.on('SIGTERM', () => {
			process.exit(2);
		});

		process.on('exit', () => {
			console.log('Exiting');
		});
	}
}

const appliaction: Application = new Application();
appliaction.initialize();
