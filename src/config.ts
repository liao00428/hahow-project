import dotenv from 'dotenv';
dotenv.config();

class Config {
	public ENVIRONMENT: string | undefined;
	public PORT: string | undefined;
	public CLIENT_HOST: string | undefined;
	public HAHOW_API_BASE_URL: string | undefined;

	constructor() {
		this.ENVIRONMENT = process.env.ENVIRONMENT;
		this.PORT = process.env.PORT;
		this.CLIENT_HOST = process.env.CLIENT_HOST;
		this.HAHOW_API_BASE_URL = process.env.HAHOW_API_BASE_URL;
	}

	public validateConfig() {
		for (const [key, value] of Object.entries(this)) {
			if (value === undefined) {
				throw new Error(`Configuration ${key} is undefined.`);
			}
		}
	}
}

export const config = new Config();
