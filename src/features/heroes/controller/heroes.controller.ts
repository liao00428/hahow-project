import { Request, Response, NextFunction } from 'express';
import HTTP_STATUS from 'http-status-codes';
import { heroesGetter } from '../services/heroesGetter.service';
import { heroGetter } from '../services/heroGetter.service';

class HeroesController {
	public async getHeroes(req: Request, res: Response, next: NextFunction) {
		try {
			const { isAuthorized } = req.body;
			const heroes = await heroesGetter.getHeroes(isAuthorized);
			res.status(HTTP_STATUS.OK).json({ status: 'success', message: 'Get hero list success', result: heroes});
		} catch (error) {
			next(error);
		}
	}

	public async getHeroById(req: Request, res: Response, next: NextFunction) {
		try {
			const { isAuthorized } = req.body;
			const { heroId } = req.params;
			const hero = await heroGetter.getHeroById(isAuthorized, heroId);
			res.status(HTTP_STATUS.OK).json({ status: 'success', message: 'Get hero by id success', result: hero });
		} catch (error) {
			next(error);
		}
	}
}

export const heroesController = new HeroesController();
