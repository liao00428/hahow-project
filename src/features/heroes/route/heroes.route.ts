import { Router } from 'express';
import { heroesController } from '../controller/heroes.controller';
import { AuthenticateMiddleware } from '../../../global/middleware/authenticate.middleware';
import { SchemaValidateMiddleware } from '../../../global/middleware/schemaValidate.middleware';
import { heroIdSchema } from '../schema/heroes.schema';

class HeroesRoute {
	private router: Router;

	constructor() {
		this.router = Router();
	}

	public routes(): Router {
		this.router.get(
			'/heroes',
			AuthenticateMiddleware.authenticate(),
			heroesController.getHeroes,
		);

		this.router.get(
			'/heroes/:heroId',
			AuthenticateMiddleware.authenticate(),
			SchemaValidateMiddleware.validateParamaters(heroIdSchema),
			heroesController.getHeroById,
		);

		return this.router;
	}
}

export const heroesRoute = new HeroesRoute();
