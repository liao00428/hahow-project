import Joi from 'joi';

export const authenticateHeaderSchema = Joi.object({
	name: Joi.string().required(),
	password: Joi.string().required()
});

export const heroIdSchema = Joi.object({
	heroId: Joi.string().pattern(/^\S+$/).required()
});
