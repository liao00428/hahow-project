import HTTP_STATUS from 'http-status-codes';
import { HeroesResourcesService, heroesResourcesService } from '../../../global/HeroesResoucesService/heroesResources.service';
import { IResult, IHero, IHeroProfile } from '../../../global/HeroesResoucesService/heroesResources.interface';
import { NotFoundError } from '../../../global/utils/customError';

export class HeroGetter {
	private heroesResourcesService: HeroesResourcesService;

	constructor(heroesResourcesService: HeroesResourcesService) {
		this.heroesResourcesService = heroesResourcesService;
	}

	/**
	 * Get hero by ID
	 * Depending on whether the user is authorized, use different strategies to get hero data.
	 * If the user is authorized, retrieve the abilities of each hero.
	 * 
	 * @param {boolean} isAuthorized - Whether the user has authorization to get hero abilities.
	 * @param {string} heroId - The ID of the hero.
	 * @returns {Promise<IHero>}
	 * Returns a hero object.
	 * For unauthorized users, this contains basic hero information.
	 * For authorized users, this includes detailed hero information.
	 */
	public async getHeroById(isAuthorized: boolean, heroId: string): Promise<IHero> {
		const heroStrategy = this.heroStrategyFactory(isAuthorized);
		const hero: IHero = await heroStrategy.getHeroById(heroId);

		return hero;
	}

	// Use the factory pattern to create different strategies.
	private heroStrategyFactory(isAuthorized: boolean) {
		switch (isAuthorized) {
			case true:
				return new AuthorizedHeroStrategy(this.heroesResourcesService);
			case false:
				return new UnAuthorizedHeroStrategy(this.heroesResourcesService);
		}
	}
}

class UnAuthorizedHeroStrategy {
	protected heroesResourcesService: HeroesResourcesService;

	constructor(heroesResourcesService: HeroesResourcesService) {
		this.heroesResourcesService = heroesResourcesService;
	}

	public async getHeroById(heroId: string): Promise<IHero> {
		const hero: IResult<IHero> = await this.heroesResourcesService.getHero(heroId);
		const { statusCode, result } = hero;
		if (statusCode === HTTP_STATUS.NOT_FOUND) { throw new NotFoundError(`Hero with id ${heroId} not found`); }

		return result as IHero;
	}
}

class AuthorizedHeroStrategy extends UnAuthorizedHeroStrategy {
	public async getHeroById(heroId: string): Promise<IHero> {
		const hero: IHero = await super.getHeroById(heroId);
		const profile: IHeroProfile = await this.getHeroProfile(hero.id);
		return { ...hero, profile };
	}

	private async getHeroProfile(heroId: string) {
		const profile: IResult<IHeroProfile> = await this.heroesResourcesService.getHeroProfile(heroId);
		const { result } = profile;

		return result as IHeroProfile;
	}
}

export const heroGetter = new HeroGetter(heroesResourcesService);
