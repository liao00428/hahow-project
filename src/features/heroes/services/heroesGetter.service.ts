import { HeroesResourcesService, heroesResourcesService } from '../../../global/HeroesResoucesService/heroesResources.service';
import { IResult, IHero, IHeroProfile } from '../../../global/HeroesResoucesService/heroesResources.interface';

export class HeroesGetter {
	private heroesResourcesService: HeroesResourcesService;

	constructor(heroesResourcesService: HeroesResourcesService) {
		this.heroesResourcesService = heroesResourcesService;
	}

	/**
	 * Get the list of heroes.
	 * Depending on whether the user is authorized, use different strategies to get hero data.
	 * If the user is authorized, retrieve the abilities of each hero.
	 * 
	 * @param {boolean} isAuthorized - Whether the user has authorization to get hero abilities.
	 * @returns {Promise<{ heroes: IHero[] }>}
	 * Returns an object containing an array of heroes.
	 * For unauthorized users, this array only contains basic hero information.
	 * For authorized users, each hero's information includes detailed hero abilities.
	 */
	public async getHeroes(isAuthorized: boolean): Promise<{ heroes: IHero[] }> {
		const heroesStrategy = this.heroesStrategyFactory(isAuthorized);
		const heroes: IHero[] = await heroesStrategy.getHeroes();

		return {
			heroes,
		};
	}

	// Use the factory pattern to create different strategies.
	private heroesStrategyFactory(isAuthorized: boolean) {
		switch (isAuthorized) {
			case true:
				return new AuthorizedHeroesStrategy(this.heroesResourcesService);
			case false:
				return new UnAuthorizedHeroesStrategy(this.heroesResourcesService);
		}
	}
}

class UnAuthorizedHeroesStrategy {
	protected heroesResourcesService: HeroesResourcesService;

	constructor(heroesResourcesService: HeroesResourcesService) {
		this.heroesResourcesService = heroesResourcesService;
	}

	public async getHeroes(): Promise<IHero[]> {
		const heroes: IResult<IHero[]> = await this.heroesResourcesService.getHeroes();
		const { result } = heroes;

		return result as IHero[];
	}
}

class AuthorizedHeroesStrategy extends UnAuthorizedHeroesStrategy {
	public async getHeroes(): Promise<IHero[]> {
		const heroes: IHero[] = await super.getHeroes();
		// Use Promise.all to concurrently get detailed information for each hero.
		const heroesWithProfile: IHero[] = await Promise.all(heroes.map(async (hero) => {
			const profile: IHeroProfile = await this.getHeroProfile(hero.id);
			return {...hero, profile };
		}));

		return heroesWithProfile;
	}

	private async getHeroProfile(heroId: string) {
		const profile: IResult<IHeroProfile> = await this.heroesResourcesService.getHeroProfile(heroId);
		const { result } = profile;

		return result as IHeroProfile;
	}
}

export const heroesGetter = new HeroesGetter(heroesResourcesService);
