import nock from 'nock';
import { config } from '../../../config';
import {  NotFoundError } from '../../../global/utils/customError';
import { heroesResourcesService, HeroesResourcesService } from '../../../global/HeroesResoucesService/heroesResources.service';
import { HeroGetter } from '../services/heroGetter.service';

const mockHeroesResourcesService = {
	getHero: jest.fn(),
	getHeroProfile: jest.fn(),
};

let heroGetter: HeroGetter;

describe('HeroGetter', () => {
	beforeEach(() => {
		jest.resetAllMocks();
		heroGetter = new HeroGetter(
      mockHeroesResourcesService as unknown as HeroesResourcesService,
		);
	});

	test('happy case : should return basic single hero if unAuthorized', async () => {
		// Arrange
		const mockHero = {
			id: '1',
			name: 'hero1',
			image: 'image1'
		};
		const mockHeroResult = {
			statusCode: 200,
			result: mockHero,
		};
		mockHeroesResourcesService.getHero.mockResolvedValue(mockHeroResult);

		// Act
		const mockIsAuthorized = false;
		const result = await heroGetter.getHeroById(mockIsAuthorized, mockHero.id);

		// Assert
		expect(result).toEqual(mockHero);
	});

	test('happy case : should return hero with full information if authorized', async () => {
		// Arrange
		const mockHero = {
			id: '1',
			name: 'hero1',
			image: 'image1'
		};
		const mockHeroResult = {
			statusCode: 200,
			result: mockHero,
		};
		mockHeroesResourcesService.getHero.mockResolvedValue(mockHeroResult);

		const mockHeroProfiles = {
			'str': 1,
			'int': 1,
			'agi': 1,
			'luk': 1,
		};
		const mockHeroProfilesResult = {
			statusCode: 200,
			result: mockHeroProfiles,
		};
		mockHeroesResourcesService.getHeroProfile.mockResolvedValue(mockHeroProfilesResult);

		// Act
		const mockIsAuthorized = true;
		const result = await heroGetter.getHeroById(mockIsAuthorized, mockHero.id);

		// Assert
		const mockHeroWithProfile = {
			...mockHero,
			profile: mockHeroProfiles,
		};
		expect(result).toEqual(mockHeroWithProfile);
	});

	test('sad case : should throw NotFoundError when hero not found', async () => {
		// Arrange
		const mockHeroResult = {
			statusCode: 404,
			error: 'Error fetching hero: Not Found',
		};
		mockHeroesResourcesService.getHero.mockResolvedValue(mockHeroResult);

		// Act and Assert
		const mockIsAuthorized = false;
		expect(heroGetter.getHeroById(mockIsAuthorized, '6')).rejects.toThrow(NotFoundError);
	});

	test('edge case : should throw NotFoundError when arguments is random string', async () => {
		// Arrange
		const mockHeroResult = {
			statusCode: 404,
			error: 'Error fetching hero: Not Found',
		};
		mockHeroesResourcesService.getHero.mockResolvedValue(mockHeroResult);

		// Act and Assert
		const mockIsAuthorized = false;
		expect(heroGetter.getHeroById(mockIsAuthorized, 'abcd')).rejects.toThrow(NotFoundError);
	});
});

describe('HeroGetter intergration test', () => {
	const baseURL = config.HAHOW_API_BASE_URL;

	beforeEach(() => {
		jest.resetAllMocks();
		heroGetter = new HeroGetter(
			heroesResourcesService
		);
	});

	test('happy case : should return basic single hero if unAuthorized', async () => {
		// Arrange
		const mockHero = {
			id: '1',
			name: 'hero1',
			image: 'http://image1'
		};
		nock(baseURL as string)
			.get(`/heroes/${mockHero.id}`)
			.reply(200, mockHero);

		// Act
		const mockIsAuthorized = false;
		const result = await heroGetter.getHeroById(mockIsAuthorized, mockHero.id);

		// Assert
		expect(result).toEqual(mockHero);
	});

	test('happy case : should return hero with full information if authorized', async () => {
		// Arrange
		const mockHero = {
			id: '1',
			name: 'hero1',
			image: 'http://image1'
		};
		nock(baseURL as string)
			.get(`/heroes/${mockHero.id}`)
			.reply(200, mockHero);

		const mockHeroProfiles = {
			'str': 1,
			'int': 1,
			'agi': 1,
			'luk': 1,
		};
		nock(baseURL as string)
			.get(`/heroes/${mockHero.id}/profile`)
			.reply(200, mockHeroProfiles);

		// Act
		const mockIsAuthorized = true;
		const result = await heroGetter.getHeroById(mockIsAuthorized, mockHero.id);

		// Assert
		const mockHeroWithProfile = {
			...mockHero,
			profile: mockHeroProfiles,
		};
		expect(result).toEqual(mockHeroWithProfile);
	});

	test('sad case : should throw NotFoundError when hero not found', async () => {
		// Arrange
		nock(baseURL as string)
			.get('/heroes/6')
			.reply(404, 'Not Found');

		// Act and Assert
		const mockIsAuthorized = false;
		expect(heroGetter.getHeroById(mockIsAuthorized, '6')).rejects.toThrow(NotFoundError);
	});

	test('sad case : should throw NotFoundError when arguments is random string', async () => {
		// Arrange
		nock(baseURL as string)
			.get('/heroes/abcd')
			.reply(404, 'Not Found');

		// Act and Assert
		const mockIsAuthorized = false;
		expect(heroGetter.getHeroById(mockIsAuthorized, 'abcd')).rejects.toThrow(NotFoundError);
	});
});
