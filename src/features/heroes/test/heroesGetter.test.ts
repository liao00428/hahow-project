import nock from 'nock';
import { config } from '../../../config';
import { heroesResourcesService, HeroesResourcesService } from '../../../global/HeroesResoucesService/heroesResources.service';
import { HeroesGetter } from '../services/heroesGetter.service';

const mockHeroesResourcesService = {
	getHeroes: jest.fn(),
	getHeroProfile: jest.fn(),
};

let heroesGetter: HeroesGetter;

describe('HeroesGetter', () => {
	beforeEach(() => {
		jest.resetAllMocks();
		heroesGetter = new HeroesGetter(
      mockHeroesResourcesService as unknown as HeroesResourcesService,
		);
	});

	test('happy case : should return basic hero list if unAuthorized', async () => {
		// Arrange
		const mockHeroes = [
			{ id: 1, name: 'hero1', image: 'image1'},
			{ id: 2, name: 'hero2', image: 'image2' },
		];
		const mockHeroesResult = {
			statusCode: 200,
			result: mockHeroes,
		};
		mockHeroesResourcesService.getHeroes.mockResolvedValue(mockHeroesResult);

		// Act
		const mockIsAuthorized = false;
		const result = await heroesGetter.getHeroes(mockIsAuthorized);

		// Assert
		expect(result).toEqual({ heroes: mockHeroes });
	});

	test('happy case : should return hero list with full information if authorized', async () => {
		// Arrange
		const mockHeroes = [
			{ id: 1, name: 'hero1', image: 'image1'},
			{ id: 2, name: 'hero2', image: 'image2' },
		];
		const mockHeroesResult = {
			statusCode: 200,
			result: mockHeroes,
		};
		mockHeroesResourcesService.getHeroes.mockResolvedValue(mockHeroesResult);

		const mockHeroProfiles = {
			'str': 1,
			'int': 1,
			'agi': 1,
			'luk': 1,
		};
		const mockHeroProfilesResult = {
			statusCode: 200,
			result: mockHeroProfiles,
		};
		mockHeroesResourcesService.getHeroProfile.mockResolvedValue(mockHeroProfilesResult);

		// Act
		const mockIsAuthorized = true;
		const result = await heroesGetter.getHeroes(mockIsAuthorized);

		// Assert
		const mockHeroesWithProfile = mockHeroes.map((hero) => {
			return {
				...hero,
				profile: mockHeroProfiles,
			};
		});
		expect(result).toEqual({ heroes: mockHeroesWithProfile });
	});
});

describe('HeroesGetter intergration test', () => {
	const baseURL = config.HAHOW_API_BASE_URL;

	beforeEach(() => {
		jest.resetAllMocks();
		heroesGetter = new HeroesGetter(
			heroesResourcesService
		);
	});

	test('happy case : should return basic hero list if unAuthorized', async () => {
		// Arrange
		const mockHeroes = [
			{ id: '1', name: 'hero1', image: 'http://image1'},
			{ id: '2', name: 'hero2', image: 'http://image2' },
		];

		nock(baseURL as string)
			.get('/heroes')
			.reply(200, mockHeroes);

		// Act
		const result = await heroesGetter.getHeroes(false);

		// Assert
		expect(result).toEqual({ heroes: mockHeroes });
	});

	test('happy case : should return hero list with full information if authorized', async () => {
		// Arrange
		const mockHeroes = [
			{ id: '1', name: 'hero1', image: 'http://image1'},
			{ id: '2', name: 'hero2', image: 'http://image2' },
		];

		const mockHeroProfiles = {
			'str': 1,
			'int': 1,
			'agi': 1,
			'luk': 1,
		};

		nock(baseURL as string)
			.get('/heroes')
			.reply(200, mockHeroes);

		mockHeroes.forEach((hero) => {
			nock(baseURL as string)
				.get(`/heroes/${hero.id}/profile`)
				.reply(200, mockHeroProfiles);
		});

		// Act
		const result = await heroesGetter.getHeroes(true);

		// Assert
		const mockHeroesWithProfile = mockHeroes.map((hero) => {
			return {
				...hero,
				profile: mockHeroProfiles,
			};
		});
		expect(result).toEqual({ heroes: mockHeroesWithProfile });
	});
});
