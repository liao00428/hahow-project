export interface  IHero {
  id: string;
  name: string;
  image: string;
  profile?: IHeroProfile;
}

export interface IHeroProfile {
  str: number;
  int: number;
  agi: number;
  luk: number;
}

export interface IResult<T> {
	statusCode: number;
	result?: T;
	error?: string;
}
