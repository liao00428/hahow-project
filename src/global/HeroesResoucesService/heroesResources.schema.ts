import Joi from 'joi';

export const heroSchema = Joi.object({
	id: Joi.string().required(),
	name: Joi.string().required(),
	image: Joi.string().uri().required(),
});

export const heroesSchema = Joi.array().items(heroSchema);

export const heroProfileSchema = Joi.object({
	str: Joi.number().required(),
	int: Joi.number().required(),
	agi: Joi.number().required(),
	luk: Joi.number().required(),
});

export const authenticationSchema = Joi.string().required();
