import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { config } from '../../config';
import { IHero, IHeroProfile, IResult } from './heroesResources.interface';
import { heroSchema, heroesSchema, heroProfileSchema, authenticationSchema } from './heroesResources.schema';
import { InternalServerError } from '../utils/customError';

export class HeroesResourcesService {
	private httpClient: AxiosInstance;

	constructor(baseURL: string) {
		this.httpClient = axios.create({
			baseURL,
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
		});
	}

	public async getHeroes(): Promise<IResult<IHero[]>> {
		try {
			const response: AxiosResponse<IHero[]> = await this.httpClient.get('/heroes');
			const { error } = heroesSchema.validate(response.data);
			if (error) { throw new Error(`Invalid response format, ${error.message}`); }

			return {
				statusCode: response.status,
				result: response.data,
			};
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return {
					statusCode: error.response.status,
					error: `Error fetching heroes: ${error.response.statusText}`,
				};
			} else {
				throw new InternalServerError(`Unexpected error fetching heroes : ${(error as Error).message}`);
			}
		}
	}

	public async getHero(heroId: string): Promise<IResult<IHero>> {
		try {
			const response: AxiosResponse<IHero> = await this.httpClient.get(`/heroes/${heroId}`);
			const { error } = heroSchema.validate(response.data);
			if (error) { throw new Error(`Invalid response format, ${error.message}`); }

			return {
				statusCode: response.status,
				result: response.data,
			};
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return {
					statusCode: error.response.status,
					error: `Error fetching hero: ${error.message}`,
				};
			} else {
				throw new InternalServerError(`Unexpected error fetching hero : ${(error as Error).message}`);
			}
		}
	}

	public async getHeroProfile(heroId: string): Promise<IResult<IHeroProfile>> {
		try {
			const response: AxiosResponse<IHeroProfile> = await this.httpClient.get(`/heroes/${heroId}/profile`);
			const { error } = heroProfileSchema.validate(response.data);
			if (error) { throw new Error(`Invalid response format, ${error.message}`); }

			return {
				statusCode: response.status,
				result: response.data,
			};
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return {
					statusCode: error.response.status,
					error: `Error fetching hero profile: ${error.response.statusText}`,
				};
			} else {
				throw new InternalServerError(`Unexpected error fetching hero profile : ${(error as Error).message}`);
			}
		}
	}

	public async getAuthentication(name: string, password: string): Promise<IResult<string>> {
		try {
			const response: AxiosResponse<string> = await this.httpClient.post('/auth', { name, password });
			const { error } = authenticationSchema.validate(response.data);
			if (error) { throw new Error(`Invalid response format, ${error.message}`); }

			return {
				statusCode: response.status,
				result: response.data,
			};
		} catch (error) {
			if (axios.isAxiosError(error) && error.response) {
				return {
					statusCode: error.response.status,
					error: `Error fetching authentication: ${error.response.statusText}`,
				};
			} else {
				throw new InternalServerError(`Unexpected error fetching authentication : ${(error as Error).message}`);
			}
		}
	}
}

const baseURL = config.HAHOW_API_BASE_URL;
export const heroesResourcesService = new HeroesResourcesService(baseURL as string);
