import HTTP_STATUS from 'http-status-codes';
import { Request, Response, NextFunction } from 'express';
import { authenticateHeaderSchema } from '../../features/heroes/schema/heroes.schema';
import { heroesResourcesService } from '../HeroesResoucesService/heroesResources.service';

export class AuthenticateMiddleware {
	static authenticate() {
		return async (req: Request, _res: Response, next: NextFunction) => {
			try {
				let isAuthorized = false;

				// headers name and password are case-insensitive
				const authenticateHeader = {
					name: req.headers['name'] || req.headers['Name'],
					password: req.headers['password'] || req.headers['Password']
				};

				const { error } = authenticateHeaderSchema.validate(authenticateHeader, { abortEarly: false });
				if (!error) {
					const { name, password } = authenticateHeader;
					const authentication = await heroesResourcesService.getAuthentication(name as string, password as string);
					const { statusCode } = authentication;

					if (statusCode === HTTP_STATUS.OK) {
						isAuthorized = true;
					}
				}
				req.body.isAuthorized = isAuthorized;

				next();
			} catch (error) {
				next(error);
			}
		};
	}
}

export const authenticateMiddleware = new AuthenticateMiddleware();
