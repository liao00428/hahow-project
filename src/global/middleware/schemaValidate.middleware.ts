import { Request, Response, NextFunction } from 'express';
import { Schema } from 'joi';
import { BadRequestError } from '../utils/customError';

export class SchemaValidateMiddleware {
	static validateParamaters(schema: Schema) {
		return (req: Request, _res: Response, next: NextFunction): void => {
			const { error } = schema.validate(req.params);
			if (error) {
				throw new BadRequestError('Bad request');
			}

			next();
		};
	}
}
