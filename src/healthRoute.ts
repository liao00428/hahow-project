import express, { Router, Request, Response } from 'express';
import HTTP_STATUS from 'http-status-codes';
import { config } from './config';

class HealthRoutes {
	private router: Router;

	constructor() {
		this.router = express.Router();
	}

	public health(): Router {
		this.router.get('/health', (_req: Request, res: Response) => {
			res.status(HTTP_STATUS.OK).send(`Health: Server instance is healthy with process id ${process.pid} on ${new Date()}`);
		});
    
		return this.router;
	}

	public env(): Router {
		this.router.get('/env', (_req: Request, res: Response) => {
			res.status(HTTP_STATUS.OK).send(`This is the ${config.ENVIRONMENT} environment`);
		});
  
		return this.router;
	}
}

export const healthRoutes: HealthRoutes = new HealthRoutes();
