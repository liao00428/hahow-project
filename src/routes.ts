import { Application } from 'express';
import { healthRoutes } from './healthRoute';
import { heroesRoute } from './features/heroes/route/heroes.route';


const BASE_PATH: string = '/api/v1';

export default (app: Application): void => {
	const routes = () => {
		app.use('', healthRoutes.health());
		app.use('', healthRoutes.env());

		app.use(BASE_PATH, heroesRoute.routes());
	};

	routes();
};
