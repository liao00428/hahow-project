import { Application, json, urlencoded, Request, Response, NextFunction } from 'express';
import http from 'http';
import cors from 'cors';
import fs from 'fs';
import path from 'path';
import YAML from 'yaml';
import swaggerUi from 'swagger-ui-express';
import applicationRoutes from './routes';
import { IErrorResponse, CustomError, NotFoundError, InternalServerError } from './global/utils/customError';
import { config } from './config';

export class HahowProjectServer {
	private app: Application;

	constructor(app: Application) {
		this.app = app;
	}

	public start(): void {
		this.securityMiddleware(this.app);
		this.standardMiddleware(this.app);
		this.performanceMiddleware(this.app);
		this.routeMiddleware(this.app);
		this.apiDocuments(this.app);
		this.errorHandler(this.app);
		this.startServer(this.app);
	}

	private securityMiddleware(app: Application): void {
		app.use(
			cors({
				origin: config.CLIENT_HOST,
				methods: ['GET']
			})
		);
	}

	private standardMiddleware(app: Application): void {
		app.use(json());
		app.use(urlencoded({ extended: true }));
	}

	private performanceMiddleware(app: Application): void {
		app.use((req: Request, res: Response, next: NextFunction) => {
			const start = performance.now();
			res.on('finish', () => {
				const end = performance.now();
				const duration = end - start;
				console.log(`${req.originalUrl} : Execution time for ${req.method} ${req.url}: ${duration.toFixed(2)} ms`);
			});
			next();
		});
	}

	private routeMiddleware(app: Application): void {
		applicationRoutes(app);
	}

	private apiDocuments(app: Application): void {
		if (config.ENVIRONMENT === 'localhost') {
			const swaggerDocuments: string[] = [];

			// Read api.yaml and components.yaml file in the apidoc folder
			const apiYamlFile: string = fs.readFileSync(path.join('apidocs', 'api.yaml'), 'utf8');
			const componentsYamlFile: string = fs.readFileSync(path.join('apidocs', 'components.yaml'), 'utf8');
			swaggerDocuments.push(YAML.parse(apiYamlFile));
			swaggerDocuments.push(YAML.parse(componentsYamlFile));

			// Read all yaml files in the apidoc/api folder
			const filePath: string = 'apidocs/api';
			const files: string[] = fs.readdirSync(filePath);
			const api = {
				paths: {}
			};
			files.forEach(file => {
				if (file.endsWith('.yaml')) {
					const yamlFile: string = fs.readFileSync(path.join(filePath, file), 'utf8');
					Object.assign(api.paths, YAML.parse(yamlFile).paths);
				}
			});

			const mergedSwaggerDocument = {};
			Object.assign(mergedSwaggerDocument, ...swaggerDocuments);
			Object.assign(mergedSwaggerDocument, api);

			app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(mergedSwaggerDocument));
		}
	}

	private errorHandler(app: Application): void {
		app.all('*', (req: Request, res: Response) => {
			const notFoundError: NotFoundError = new NotFoundError(`${req.originalUrl} not found`);
			res.status(notFoundError.statusCode).json(notFoundError.serializeErrors());
		});

		app.use((err: IErrorResponse, _req: Request, res: Response, next: NextFunction) => {
			if (err instanceof CustomError) {
				console.log([err.statusCode, err.message, err.stack.split('\n')[1].trim()]);
				res.status(err.statusCode).json(err.serializeErrors());
			} else {
				const internalServerError: InternalServerError = new InternalServerError(err.message);
				console.log([internalServerError.statusCode, err.message, err.stack.split('\n')[1].trim()]);
				res.status(internalServerError.statusCode).json(internalServerError.serializeErrors());
			}

			next();
		});
	}

	private startServer(app: Application): void {
		const httpServer: http.Server = new http.Server(app);
		this.startHttpServer(httpServer);
	}

	private startHttpServer(httpServer: http.Server): void {
		httpServer.listen(config.PORT, () => {
			console.log(`Server is running on port ${config.PORT}`);
		});
	}
}
